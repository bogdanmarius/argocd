# ArgoCD repository

# Requirements for local setup of ArgoCD
Requirements for minikube setup:
<p>docker</p>
<p>minikube</p>
<p>kubectl repo and tool installed</p>

# installation of ArgoCD
# install ArgoCD in k8s
kubectl create namespace argocd
<p>kubectl apply -n argocd -f https://raw.githubusercontent.com/argoproj/argo-cd/stable/manifests/install.yaml</p>

# access ArgoCD UI
kubectl get svc -n argocd
<p>kubectl port-forward svc/argocd-server 9090:443 -n argocd</p>

# login with admin user and below token (as in documentation):
kubectl -n argocd get secret argocd-initial-admin-secret -o jsonpath="{.data.password}" | base64 --decode && echo

# you can change and delete init password
this is a good security practice for production environment(s)

# repository usage
Add the repo to the argocd web interface:
<p>https://gitlab.com/bogdanmarius/argocd.git</p>

# management interface access
https://localhost:9090/

# my local guide
https://gitlab.com/bogdanmarius/commands/-/blob/master/my-argocd-commands.txt

# External resources
<p>Install ArgoCD: https://argo-cd.readthedocs.io/en/stable/getting_started/#1-install-argo-cd</p>

<p>Login to ArgoCD: https://argo-cd.readthedocs.io/en/stable/getting_started/#4-login-using-the-cli</p>

<p>ArgoCD Configuration: https://argo-cd.readthedocs.io/en/stable/operator-manual/declarative-setup/</p>